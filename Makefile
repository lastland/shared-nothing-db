all:
	cd Shared-Nothing-DB && make

clean:
	cd Shared-Nothing-DB && make clean

.PHONY: all clean
