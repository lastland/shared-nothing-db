//
//  Request.h
//  Shared-Nothing-DB
//
//  Created by lastland on 14-4-6.
//  Copyright (c) 2014年 lastland. All rights reserved.
//

#ifndef __Shared_Nothing_DB__Request__
#define __Shared_Nothing_DB__Request__

#include <string>
#include <vector>
#include <list>
#include <mutex>
#include <condition_variable>
#include "Common.h"
#include "Result.h"

#define RequestContent \
    RequestType tpe; \
    int arg1; \
    int arg2; \
    int arg3; \
    int arg4; \
    int arg5; \
    char* content;

struct Request {
    RequestContent
};

struct RequestWithID {
    clientIdType clientId;
    RequestContent
};

struct RequestWithRID {
    idType tid;
    idType rid;
    clientIdType clientId;
    RequestContent
};

struct SubscribeRequest {
    clientIdType clientId;
    std::mutex *mutex;
    std::condition_variable *condVar;
    std::vector<std::string> **resBuf;
    TransactionResult *tranRes;
    int *tranReqsNum;
    bool *resFlag;
};

struct TransactionRequest {
    clientIdType clientId;
    std::list<Request*> *reqs;
};

#endif /* defined(__Shared_Nothing_DB__Request__) */
