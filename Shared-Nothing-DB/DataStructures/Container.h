//
//  Container.h
//  Shared-Nothing-DB
//
//  Created by lastland on 14-4-8.
//  Copyright (c) 2014年 lastland. All rights reserved.
//

#ifndef __Shared_Nothing_DB__Container__
#define __Shared_Nothing_DB__Container__

#include <vector>

template <typename T>
class Container {
public:
    virtual bool get(int n, T& res) = 0;
    virtual std::vector<T> getRange(int n, int m) = 0;
    virtual bool set(int n, T content) = 0;
};

#endif /* defined(__Shared_Nothing_DB__Container__) */
