//
//  BTree.h
//  Shared-Nothing-DB
//
//  Created by lastland on 14-4-8.
//  Copyright (c) 2014年 lastland. All rights reserved.
//

#ifndef __Shared_Nothing_DB__BTree__
#define __Shared_Nothing_DB__BTree__

#define GETRANGE_OPT

#include <vector>
#include "Container.h"

const unsigned maxNChildren = 24;

template<typename T>
class BTree: Container<T> {
    struct Node {
        unsigned nKeys;
        int keys[maxNChildren - 1];
        T *values[maxNChildren - 1];
        Node *children[maxNChildren];
        bool leaf;
    };
    Node *root;
    // Don't put following methods into struct Node!
    void createNode(Node** node);
    T** searchNode(Node* node, int n);
    void splitChild(Node* parent, unsigned p, Node* child);
    void releaseNode(Node** node);
    
    bool insert(int k, T* v);
    bool _insert(Node* node, int k, T* v);
#ifdef GETRANGE_OPT	
	void _getAll(Node* node, std::vector<T>& rtv);
	void _getRange(int n, int m, Node* node, std::vector<T>& rtv);
#endif
public:
    BTree();
    ~BTree();
    virtual bool get(int n, T& res);
    virtual std::vector<T> getRange(int n, int m);
    virtual bool set(int n, T content);
};


template <typename T>
BTree<T>::BTree() {
    createNode(&root);
}


template <typename T>
BTree<T>::~BTree() {
    releaseNode(&root);
}


template <typename T>
void BTree<T>::createNode(Node **node) {
    *node = new Node;
    (*node)->leaf = true;
    (*node)->nKeys = 0;
}


template <typename T>
void BTree<T>::releaseNode(Node **node) {
    if (!(*node)->leaf)
        for (int i = 0; i <= (*node)->nKeys; i++)
            releaseNode(&(*node)->children[i]);
    for (int i = 0; i < (*node)->nKeys; i++)
        delete (*node)->values[i];
    delete *node;
}


template <typename T>
T** BTree<T>::searchNode(Node* node, int n) {
    unsigned i;
    for (i = 0; i < node->nKeys && node->keys[i] < n; i++);
    if (i < node->nKeys && node->keys[i] == n)
        return &node->values[i];
    if (node->leaf)
        return nullptr;
    return searchNode(node->children[i], n);
}


template <typename T>
void BTree<T>::splitChild(Node *parent, unsigned int p, Node *child) {
    unsigned t = child->nKeys / 2;
    Node* newChild = new Node;
    
    newChild->leaf = child->leaf;
    newChild->nKeys = child->nKeys = t;
    
    for (int i = 0; i < t; i++) {
        newChild->keys[i] = child->keys[i + t + 1];
        newChild->values[i] = child->values[i + t + 1];
    }
    if (!newChild->leaf)
        for (int i = 0; i <= t; i++)
            newChild->children[i] = child->children[i + t + 1];
    
    for (int i = parent->nKeys + 1; i > p + 1; i--)
        parent->children[i] = parent->children[i - 1];
    parent->children[p + 1] = newChild;
    
    for (int i = parent->nKeys; i > p; i--) {
        parent->keys[i] = parent->keys[i - 1];
        parent->values[i] = parent->values[i - 1];
    }
    
    parent->keys[p] = child->keys[t];
    parent->values[p] = child->values[t];
    parent->nKeys++;
}


template <typename T>
bool BTree<T>::insert(int k, T* v) {
    if (maxNChildren - 1 == root->nKeys) {
        Node* newRoot;
        createNode(&newRoot);
        newRoot->leaf = false;
        newRoot->children[0] = root;
        splitChild(newRoot, 0, root);
        root = newRoot;
    }
    return _insert(root, k, v);
}


template <typename T>
bool BTree<T>::_insert(Node* node, int k, T* v) {
    if (node->leaf) {
        int i;
        for (i = node->nKeys - 1; i >= 0 && node->keys[i] > k; i--) {
            node->keys[i + 1] = node->keys[i];
            node->values[i + 1] = node->values[i];
        }
        node->keys[i + 1] = k;
        node->values[i + 1] = v;
        node->nKeys++;
        return true;
    } else {
        unsigned i;
        for (i = 0; i < node->nKeys && node->keys[i] < k; i++);
        Node *child = node->children[i];
        if (child->nKeys == maxNChildren - 1) {
            splitChild(node, i, child);
            if (node->keys[i] < k) child = node->children[i + 1];
        }
        return _insert(child, k, v);
    }
}


template <typename T>
bool BTree<T>::get(int n, T& res) {
    T** r = searchNode(root, n);
    if (nullptr == r) return false;
    res = **r;
    return true;
}


#ifdef GETRANGE_OPT
template <typename T>
void BTree<T>::_getAll(Node* node, std::vector<T>& rtv) {
	unsigned i;
	bool isLeaf = node->leaf;
	for (i = 0; i < node->nKeys; i++) {
		if(!isLeaf)
			_getAll(node->children[i], rtv);
		T** r = &node->values[i];
		rtv.push_back(**r);
	}
	if (!isLeaf)
		_getAll(node->children[i], rtv);
}

template <typename T>
void BTree<T>::_getRange(int n, int m, Node* node, std::vector<T>& rtv) {
	if (n > m)
		return;
    unsigned i;
	bool found = false;
    for (i = 0; i < node->nKeys && node->keys[i] < n; i++);
    if (i < node->nKeys && node->keys[i] == n) {
		T** r = &node->values[i];
        rtv.push_back(**r); // get(n)
		found = true;
	}
    else if (node->leaf) // n not found, try n+1 from root (since we cannot back trace parent node)
	{
        return _getRange(n+1, m, root, rtv);
	}
	else if (!found) { // search down
		return _getRange(n, m, node->children[i], rtv);
	}

	// get(n) found
	for (++i; i < node->nKeys && node->keys[i] <=m; i++) {
		if (!node->leaf) {
			_getAll(node->children[i], rtv);  // getAll(children(i-1, i))
		}
		T** r = &node->values[i];
		rtv.push_back(**r); // get(i), n < i < m
		// if get(m) is checked, finish getRange
		if (node->keys[i] == m) {
			return;
		}
	}
	if (i < node->nKeys && node->keys[i] > m) { // m is passed
		// if not leaf, only need to go through children
		if (node->leaf) {
			return;
		}
		else
			return _getRange(node->keys[i-1]+1, m, node->children[i], rtv);
			//return _getRange(node->keys[i-1]+1, m, root, rtv);
	}
	if (i >= node->nKeys) { // last key in the node (keys[nKeys-1]) < m
		// Seems that we need to both check the last child and maybe its parent for (lastkey+1,m)..
		return _getRange(node->keys[i-1]+1, m, root, rtv);
	}
}
#endif


template <typename T>
std::vector<T> BTree<T>::getRange(int n, int m) {
#ifndef GETRANGE_OPT
	std::vector<T> rtv;
    for (int i = n; i <= m; i++)
    {
        T** r = searchNode(root, i);
        if (nullptr != r)
            rtv.push_back(**r);
    }
    return rtv;
#else
	std::vector<T> rtv;
	_getRange(n, m, root, rtv);
	return rtv;
#endif
}


template <typename T>
bool BTree<T>::set(int n, T content) {
    T** v = searchNode(root, n);
    if (nullptr != v) {
        delete *v;
        *v = new T(content);
        return true;
    } else {
        return insert(n, new T(content));
    }
}


#endif /* defined(__Shared_Nothing_DB__BTree__) */
