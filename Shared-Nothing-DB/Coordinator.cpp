//
//  Coordinator.cpp
//  Shared-Nothing-DB
//
//  Created by lastland on 14-4-6.
//  Copyright (c) 2014年 lastland. All rights reserved.
//

#include <mutex>
#include <condition_variable>
#include <iterator>
#include <algorithm>
#include "Worker.h"
#include "Coordinator.h"

const unsigned maxNSubscriber = 512;
const unsigned preallocQueueLength = 6400;


Coordinator::Coordinator(unsigned nWorkers, int range, bool equalShared):
    m_msgFromClients(maxNSubscriber), m_subscribers(maxNSubscriber),  m_tranFromClients(maxNSubscriber) {
    m_nWorkers = nWorkers;
    m_equalShared = equalShared;
    m_range = range;
    m_rid = 0;
    m_tid = 1;
    for (unsigned i = 0; i < nWorkers; i++) {
        moodycamel::ReaderWriterQueue<RequestWithRID> *reqs =
            new moodycamel::ReaderWriterQueue<RequestWithRID>(preallocQueueLength);
        moodycamel::ReaderWriterQueue<CommonResultWithWorkerID> *ress =
            new moodycamel::ReaderWriterQueue<CommonResultWithWorkerID>(preallocQueueLength);
        std::cout<<"initing thread"<<i<<std::endl;
        std::thread *t = new std::thread(work, i, i * (range / nWorkers), (i + 1) * (range / nWorkers) - 1, reqs, ress);
        std::cout<<"thread "<<i<<" done"<<std::endl;
        m_workers.push_back(t);
        m_msgToWorkers.push_back(reqs);
        m_msgFromWorkers.push_back(ress);
    }
}

idType Coordinator::getRid() {
    return m_rid++;
}

idType Coordinator::getTid() {
    return m_tid++;
}

void Coordinator::handleRequest(RequestWithRID req) {
    int n, m;
    switch (req.tpe) {
        case Get:
        case Set:
            n = req.arg1 / (m_range / m_nWorkers);
            m_msgToWorkers[n]->enqueue(req);
            break;
        case GetRange:
            n = req.arg1 / (m_range / m_nWorkers);
            m = req.arg2 / (m_range / m_nWorkers);
            for (int i = n; i <= m; i++)
            {
                req.arg3 = i - n;
                req.arg4 = m - n + 1;
                req.arg5 = (n + 1) * (m_range / m_nWorkers) - req.arg1;
                m_msgToWorkers[i]->enqueue(req);
            }
            break;
    }
}

void Coordinator::packTranRes(CommonResultWithWorkerID res) {
    auto resBuf = m_resBufs.find(res.clientId)->second;
    auto i = m_tranRess.find(res.tid);
    if (i == m_tranRess.end())
        m_tranRess[res.tid] = new std::vector<std::pair<idType, std::vector<std::string>>>();
    auto v = m_tranRess.find(res.tid)->second;
    v->push_back(std::pair<idType, std::vector<std::string>> (res.rid, *resBuf));
    int reqsNum = *m_subInfos[res.clientId].tranReqsNum;
    if (v->size() == reqsNum) {
        std::sort(v->begin(), v->end(),
                  [](std::pair<idType, std::vector<std::string>> a,
                     std::pair<idType, std::vector<std::string>> b) {
                      return b.first < a.first;
                  });
        auto tranRes = m_subInfos[res.clientId].tranRes;
        for (auto e: *v)
            tranRes->push_front(e.second);
        m_subInfos[res.clientId].condVar->notify_one();
        v->clear();
    }
}

void Coordinator::run() {
    while (true) {

        // handle subscribers
        while (!m_subscribers.empty()) {
            SubscribeRequest req;
            bool flag = m_subscribers.pop(req);
            if (!flag) break;
            m_subInfos[req.clientId] = req;
            std::unique_lock<std::mutex> lock(*req.mutex);
            req.condVar->notify_one();
        }
        
        // handle transactions
        while (true) {
            TransactionRequest trans;
            bool f = m_tranFromClients.pop(trans);
            if (!f) break;
            idType tid = getTid();
            for (auto req: *trans.reqs) {
                handleRequest(RequestWithRID {
                    tid, getRid(), trans.clientId, req->tpe,
                    req->arg1, req->arg2, req->arg3,
                    req->arg4, req->arg5, req->content
                });
            }
        }

        // handle single requests
        while (true) {
            RequestWithID req;
            bool f = m_msgFromClients.pop(req);
            if (!f) break;
            handleRequest(RequestWithRID {
                0, getRid(), req.clientId, req.tpe,
                req.arg1, req.arg2, req.arg3,
                req.arg4, req.arg5, req.content
            });
        }

        for (int i = 0; i < m_nWorkers; i++) {
            // TODO: gather GetRange results
            moodycamel::ReaderWriterQueue<CommonResultWithWorkerID> *queue = m_msgFromWorkers[i];
            while (true) {
                CommonResultWithWorkerID res;
                bool f = queue->try_dequeue(res);
                if (!f) break;
                bool fi = true;
                if (m_resBufs.find(res.clientId) == m_resBufs.end())
                    m_resBufs[res.clientId] = new std::vector<std::string>();
                auto resBuf = m_resBufs.find(res.clientId)->second;
                switch (res.tpe) {
                    case Get:
                        resBuf->resize(1);
                        resBuf->at(0) = res.res[0];
                        break;
                    case Set:
                        resBuf->resize(1);
                        resBuf->at(0) = "-true";  // there should be a better way...
                        break;
                    case GetRange:
                        auto it = m_progressBufs.find(res.rid);
                        if (it == m_progressBufs.end())
                        {
                            m_progressBufs[res.rid] = new std::vector<bool>(res.nParts);
                            auto progressBuf = m_progressBufs.find(res.rid)->second;
                            for (int j = 0; j < res.nParts; j++)
                                progressBuf->at(j) = false;
                            resBuf->resize(res.totalLen);
                        }
                        auto progressBuf = m_progressBufs.find(res.rid)->second;
                        progressBuf->at(res.part) = true;
                        for (int j = 0; j < res.res.size(); j++)
                        {
                            if (res.part > 0)
                                resBuf->at(j + res.firstLen + (res.part - 1) * (m_range / m_nWorkers)) = res.res[j];
                            else
                                resBuf->at(j) = res.res[j];
                        }
                        for (int j = 0; j < res.nParts; j++)
                        {
                            if (!progressBuf->at(j))
                                fi = false;
                        }
                        break;
                }
                std::unique_lock<std::mutex> lock(*m_subInfos[res.clientId].mutex);
                if (res.tpe != GetRange) {
                    if (!res.tid) {
                        *(m_subInfos[res.clientId].resBuf) = new std::vector<std::string> (*resBuf);
                        m_subInfos[res.clientId].condVar->notify_one();
                    } else {
                        packTranRes(res);
                    }
                }
                else if (fi) {
                    if (!res.tid) {
                        *(m_subInfos[res.clientId].resBuf) = new std::vector<std::string> (*resBuf);
                        m_subInfos[res.clientId].condVar->notify_one();
                    } else {
                        packTranRes(res);
                    }
                }
            }
        }
    }
}

void run(int nWorkers, int range, bool equalShared) {
    Coordinator c(nWorkers, range, equalShared);
    c.run();
}
