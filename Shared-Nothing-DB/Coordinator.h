//
//  Coordinator.h
//  Shared-Nothing-DB
//
//  Created by lastland on 14-4-6.
//  Copyright (c) 2014年 lastland. All rights reserved.
//

#ifndef __Shared_Nothing_DB__Coordinator__
#define __Shared_Nothing_DB__Coordinator__

#include <vector>
#include <map>
#include <unordered_map>
#include <iterator>
#include <thread>
#include <mutex>
#include <utility>
#include <condition_variable>
#include <boost/lockfree/queue.hpp>

#include "Common.h"
#include "Request.h"
#include "Result.h"
#include "readerwriterqueue/readerwriterqueue.h"

class Coordinator {
    int m_nWorkers;
    int m_range;
    bool m_equalShared;
    
    idType m_rid;  // start from 0
    idType m_tid;  // start from 1
    
    std::vector<std::thread *> m_workers;
    std::vector<moodycamel::ReaderWriterQueue<RequestWithRID> *> m_msgToWorkers;
    std::vector<moodycamel::ReaderWriterQueue<CommonResultWithWorkerID> *> m_msgFromWorkers;
    std::map<clientIdType, SubscribeRequest> m_subInfos;
    std::unordered_map<idType, progressBarType*> m_progressBufs; // per rid
    std::unordered_map<idType, std::vector<std::pair<idType, std::vector<std::string>>>*> m_tranRess; // per tid
    std::unordered_map<clientIdType, std::vector<std::string>*> m_resBufs; // per client id
    
    void handleRequest(RequestWithRID req);
    
    void packTranRes(CommonResultWithWorkerID res);
    
    idType getRid();
    idType getTid();

public:
    Coordinator(unsigned nWorkers, int range, bool equalShared = true);
    void request(Request req);
    void run();
    
    boost::lockfree::queue<SubscribeRequest> m_subscribers;
    boost::lockfree::queue<RequestWithID> m_msgFromClients;
    boost::lockfree::queue<TransactionRequest> m_tranFromClients;
};

void runCoordinator(int nWorkers, int range, bool equalShared = true);

#endif /* defined(__Shared_Nothing_DB__Coordinator__) */
