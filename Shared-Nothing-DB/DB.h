//
//  DB.h
//  Shared-Nothing-DB
//
//  Created by lastland on 14-4-7.
//  Copyright (c) 2014年 lastland. All rights reserved.
//

#ifndef __Shared_Nothing_DB__DB__
#define __Shared_Nothing_DB__DB__

#include <thread>
#include <vector>
#include <string>
#include <mutex>
#include <condition_variable>
#include "Coordinator.h"
#include "Exceptions.h"

class DB {
    Coordinator *m_c;
    std::mutex m_mutex;
    std::condition_variable m_condVar;
    bool m_subscribed;
    bool subscribe(void);
    bool m_isTransaction;
    int m_tranReqsNum;
    TransactionRequest m_tranReqs;
    TransactionResult m_tranRes;
public:
    DB(Coordinator* c);
    std::string get(int n);
    std::vector<std::string> getRange(int start, int end);
    bool set(int n, std::string content);
    
    void startTransaction();
    void transactionGet(int n);
    void transactionSet(int n, std::string content);
    void transactionGetRange(int start, int end);
    TransactionResult commitTransaction();

    std::vector<std::string> *resContent;
    std::vector<bool> *resProgress;
    bool resFlag;
};

#endif /* defined(__Shared_Nothing_DB__DB__) */
