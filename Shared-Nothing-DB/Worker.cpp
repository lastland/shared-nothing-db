//
//  Worker.cpp
//  Shared-Nothing-DB
//
//  Created by lastland on 14-4-7.
//  Copyright (c) 2014年 lastland. All rights reserved.
//

#include "DataStructures/Container.h"
#include "DataStructures/BTree.h"
#include "Worker.h"

#include <map>
#include <string>

using namespace moodycamel;

using namespace std;

void work(workerIdType id,
          int start,
          int end,
          ReaderWriterQueue<RequestWithRID> *reqs,
          ReaderWriterQueue<CommonResultWithWorkerID> *ress) {

    RequestWithRID req;
    string s;

    BTree<std::string> data;

    while (true) {
        vector<string> v;
        if (reqs->try_dequeue(req)) {
            switch (req.tpe) {
                case Get:
                    if (data.get(req.arg1, s))
                        v.push_back(s);
                    else
                        v.push_back("");
                    break;
                case Set:
                    data.set(req.arg1, std::string(req.content));
                    delete req.content;
                    break;
                case GetRange:
                    int n = req.arg1 < start ? start : req.arg1;
                    int m = req.arg2 > end ? end : req.arg2;
                    v = data.getRange(n, m);
                    break;
            }
            ress->enqueue(CommonResultWithWorkerID {
                req.tid, req.rid, id, req.clientId, req.tpe, req.arg3, req.arg4, req.arg2 - req.arg1 + 1, req.arg5, v
            });
        }
    }
}
