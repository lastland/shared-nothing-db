//
//  Exceptions.h
//  Shared-Nothing-DB
//
//  Created by lastland on 14-6-13.
//  Copyright (c) 2014年 lastland. All rights reserved.
//

#ifndef Shared_Nothing_DB_Exceptions_h
#define Shared_Nothing_DB_Exceptions_h

#include <exception>

class TransactionAlreadyStarted: public std::exception {
    virtual const char* what() const throw()
    {
        return "Transaction has already started!";
    }
};

class TransactionNotStarted: public std::exception {
    virtual const char* what() const throw()
    {
        return "Transaction has not yet started!";
    }
};

#endif