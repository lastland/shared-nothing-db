//
//  main.cpp
//  Shared-Nothing-DB
//
//  Created by lastland on 14-4-6.
//  Copyright (c) 2014年 lastland. All rights reserved.
//

#include <iostream>
#include <sstream>
#include <random>
#include <ctime>
#include "DB.h"
#include "Coordinator.h"

const unsigned workerNum = 4;
const unsigned threadNum = 4;
const unsigned nQuery = 1000;
const unsigned maxNum = nQuery * workerNum;
const std::string trueStr = "-true";

void singleRequestTest() {
    std::random_device rd;
    std::mt19937 rng(rd());
    std::uniform_int_distribution<> dis(0, maxNum);
    std::uniform_int_distribution<> dis2(0, maxNum - 1);

    Coordinator *c = new Coordinator(workerNum, maxNum);
    std::thread t([&] () { c->run(); } );
    std::thread ts[workerNum];
    std::string expected[maxNum];
    DB *db1 = new DB(c);
    for (int i = 0; i < maxNum; i++) {
        std::stringstream s;
        s<<dis(rng)<<std::endl;
        expected[i] = s.str();
        db1->set(i, expected[i]);
    }
    clock_t startTime = clock();
    time_t start = time(0);
    for (unsigned wid = 0; wid < threadNum; wid++) {
        ts[wid] = std::thread([&] (unsigned id) {
            DB *db = new DB(c);
            for (int i = 0; i < nQuery; i++) {
                db->set(id * nQuery + i, expected[id * nQuery + i]);
            }
            for (int i = 0; i < nQuery; i++) {
                auto res = db->get(id * nQuery + i);
                if (expected[id * nQuery + i] != res) {
                    std::cout<<"Client "<<id
                             <<": Error!\nExpected: "<<expected[id * nQuery + i]
                             <<", got: "<<res<<std::endl;
                    return;
                }
            }
            for (int i = 0; i < nQuery * 0.1; i++) {
                int a = dis2(rng), b = dis2(rng);
	    		if (a > b) std::swap(a, b);
                auto res = db->getRange(a, b);
                for (int j = 0; j < b - a + 1; j++)
                {
                    if (expected[j + a] != res[j]) {
                        std::cout<<"getrange "<<a<<" "<<b<<std::endl;
                        std::cout<<"Client "<<id<<" j="<<j
                             <<": Error!\nExpected: "<<expected[j + a]
                             <<", got: "<<res[j]<<std::endl;
                        return;
                    }
                }
            }
            std::cout<<"Client "<<id<<": Passed!\n";
        }, wid);
    }
    for (unsigned wid = 0; wid < threadNum; wid++) {
        ts[wid].join();
    }
    t.detach();
    time_t end = time(0);
    clock_t endTime = clock();
    std::cout<<"Total time = "<<(float)(endTime - startTime) / CLOCKS_PER_SEC<<"s or "<<difftime(end, start)<<std::endl;
}

void transactionRequestTest() {
    std::random_device rd;
    std::mt19937 rng(rd());
    std::uniform_int_distribution<> dis(0, maxNum);
    std::uniform_int_distribution<> dis2(0, maxNum - 1);
    
    Coordinator *c = new Coordinator(workerNum, maxNum);
    std::thread t([&] () { c->run(); } );
    std::thread ts[workerNum];
    std::string expected[maxNum];
    std::string expectedThen[maxNum];
    for (int i = 0; i < maxNum; i++) {
        std::stringstream s;
        s<<dis(rng)<<std::endl;
        expected[i] = s.str();
        s<<dis(rng)<<std::endl;
        expectedThen[i] = s.str();
    }
    clock_t startTime = clock();
    time_t start = time(0);
    for (unsigned wid = 0; wid < threadNum; wid++) {
        ts[wid] = std::thread([&] (unsigned id) {
            DB *db = new DB(c);
            int nextwid = (wid + 1) % threadNum;
            std::string exp[8] = {
                trueStr, trueStr, expected[wid], expected[nextwid],
                trueStr, trueStr, expectedThen[wid], expectedThen[nextwid]
            };
            for (int i = 0; i < nQuery; i++) {
                db->startTransaction();
                db->transactionSet(wid, expected[wid]);
                db->transactionSet(nextwid, expected[nextwid]);
                db->transactionGet(wid);
                db->transactionGet(nextwid);
                db->transactionSet(wid, expectedThen[wid]);
                db->transactionSet(nextwid, expectedThen[nextwid]);
                db->transactionGet(wid);
                db->transactionGet(nextwid);
                TransactionResult ress = db->commitTransaction();
                int j = 0;
                for (auto res: ress) {
                    if (res[0] != exp[j++]) {
                        for (auto res: ress) std::cout<<res[0]<<' ';
                        std::cout<<"\nWrong! expect "<<exp[j - 1]<<" on "<<j-1
                                 <<", got "<<res[0]<<std::endl;
                        return;
                    }
                }
            }
            std::cout<<"Client "<<id<<": Passed!\n";
        }, wid);
    }
    for (unsigned wid = 0; wid < threadNum; wid++) {
        ts[wid].join();
    }
    t.detach();
    time_t end = time(0);
    clock_t endTime = clock();
    std::cout<<"Total time = "<<(float)(endTime - startTime) / CLOCKS_PER_SEC<<"s or "<<difftime(end, start)<<std::endl;
}

int main(int argc, const char * argv[]) {
    transactionRequestTest();
}