//
//  DB.cpp
//  Shared-Nothing-DB
//
//  Created by lastland on 14-4-7.
//  Copyright (c) 2014年 lastland. All rights reserved.
//

#include "DB.h"

#define FuncHead \
    if (!m_subscribed) subscribe(); \
    if (nullptr != resContent) { \
        delete resContent; \
        resContent = nullptr; \
    } \
    if (nullptr != resProgress) { \
        delete resProgress; \
        resProgress = nullptr; \
    } \
    resFlag = false;

#define TransCheck \
    if (!m_isTransaction) \
        throw TransactionNotStarted();

DB::DB(Coordinator *c) {
    m_c = c;
    m_subscribed = false;
    m_isTransaction = false;
    m_tranReqs.reqs = new std::list<Request*>;
}

bool DB::subscribe(void) {
    std::unique_lock<std::mutex> lock(m_mutex);
    m_c->m_subscribers.push(SubscribeRequest {
        std::this_thread::get_id(),
        &m_mutex,
        &m_condVar,
        &resContent,
        &m_tranRes,
        &m_tranReqsNum,
        &resFlag
    });
    m_condVar.wait(lock);
    m_subscribed = true;
    return m_subscribed;
}

std::string DB::get(int n) {
    FuncHead;
    std::unique_lock<std::mutex> lock(m_mutex);
    m_c->m_msgFromClients.push(RequestWithID {
        std::this_thread::get_id(), Get, n, 0, 0, 0, 0, nullptr
    });
    m_condVar.wait(lock);
    return (*resContent)[0];
}

std::vector<std::string> DB::getRange(int start, int end) {
    FuncHead;
    std::unique_lock<std::mutex> lock(m_mutex);
    m_c->m_msgFromClients.push(RequestWithID {
        std::this_thread::get_id(), GetRange, start, end, 0, 0, 0, nullptr
    });
    m_condVar.wait(lock);
    return *resContent;
}

bool DB::set(int n, std::string content) {
    FuncHead;
    char* s = new char[content.length()];
    strcpy(s, content.c_str());

    std::unique_lock<std::mutex> lock(m_mutex);
    m_c->m_msgFromClients.push(RequestWithID {
        std::this_thread::get_id(), Set, n, 0, 0, 0, 0, s
    });
    m_condVar.wait(lock);
    return resFlag;
}

void DB::startTransaction() {
    if (!m_isTransaction) {
        m_tranReqs.clientId = std::this_thread::get_id();
        m_tranReqs.reqs->clear();
        m_tranRes.clear();
        m_tranReqsNum = 0;
        m_isTransaction = true;
    }
    else throw TransactionAlreadyStarted();
}

void DB::transactionGet(int n) {
    TransCheck;
    m_tranReqs.reqs->push_back(new Request {
        Get, n, 0, 0, 0, 0, nullptr
    });
    m_tranReqsNum++;
}

void DB::transactionSet(int n, std::string content) {
    TransCheck;
    char* s = new char[content.length()];
    strcpy(s, content.c_str());
    m_tranReqs.reqs->push_back(new Request {
         Set, n, 0, 0, 0, 0, s
    });
    m_tranReqsNum++;
}

void DB::transactionGetRange(int start, int end) {
    TransCheck;
    m_tranReqs.reqs->push_back(new Request {
        GetRange, start, end, 0, 0, 0, nullptr
    });
    m_tranReqsNum++;
}

TransactionResult DB::commitTransaction() {
    TransCheck;
    FuncHead;
    std::unique_lock<std::mutex> lock(m_mutex);
    m_c->m_tranFromClients.push(m_tranReqs);
    m_condVar.wait(lock);
    m_isTransaction = false;
    return m_tranRes;
}