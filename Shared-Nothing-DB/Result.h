//
//  Result.h
//  Shared-Nothing-DB
//
//  Created by lastland on 14-4-7.
//  Copyright (c) 2014年 lastland. All rights reserved.
//

#ifndef __Shared_Nothing_DB__Result__
#define __Shared_Nothing_DB__Result__

#include <string>
#include <forward_list>
#include "Common.h"
#include "Request.h"

#define CommonResultContent \
    RequestType tpe; \
    int part; \
    int nParts; \
    int totalLen; \
    int firstLen; \
    std::vector<std::string> res;

struct CommonResult {
    CommonResultContent
};

struct CommonResultWithWorkerID {
    idType tid;
    idType rid;
    workerIdType workerId;
    clientIdType clientId;
    CommonResultContent
};

struct CommonResultWithID {
    clientIdType clientId;
    CommonResultContent
};

typedef std::forward_list<std::vector<std::string>> TransactionResult;

#endif /* defined(__Shared_Nothing_DB__Result__) */
