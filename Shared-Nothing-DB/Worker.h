//
//  Worker.h
//  Shared-Nothing-DB
//
//  Created by lastland on 14-4-7.
//  Copyright (c) 2014年 lastland. All rights reserved.
//

#ifndef __Shared_Nothing_DB__Worker__
#define __Shared_Nothing_DB__Worker__

#include "Common.h"
#include "Request.h"
#include "Result.h"
#include "readerwriterqueue/readerwriterqueue.h"

void work(workerIdType id,
          int start,
          int end,
          moodycamel::ReaderWriterQueue<RequestWithRID> *reqs,
          moodycamel::ReaderWriterQueue<CommonResultWithWorkerID> *ress);

#endif /* defined(__Shared_Nothing_DB__Worker__) */
