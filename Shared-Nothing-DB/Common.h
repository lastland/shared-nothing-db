//
//  Common.h
//  Shared-Nothing-DB
//
//  Created by lastland on 14-4-7.
//  Copyright (c) 2014年 lastland. All rights reserved.
//

#ifndef __Shared_Nothing_DB__Common__
#define __Shared_Nothing_DB__Common__

#include <thread>
#include <vector>

enum RequestType {
    Get,
    GetRange,
    Set
};

typedef std::thread::id clientIdType;
typedef unsigned idType;
typedef unsigned workerIdType;

typedef std::vector<bool> progressBarType;

#endif /* defined(__Shared_Nothing_DB__Common__) */